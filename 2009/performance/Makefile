LATEX=pdflatex

all: performance.pdf

performance.pdf: node-selection/exit-capacity.pdf \
	node-selection/optimum-selection-probabilities.pdf \
	node-selection/relative-selection-probabilities.pdf \
	node-selection/vary-network-load.pdf \
	equilibrium.pdf \
	performance.bbl

%.pdf %.aux: %.tex
	rm -f $*.ps $*.pdf
	$(LATEX) $< || { rm -f $*.pdf $*.dvi $*.aux $*.idx && false ; }
	while grep 'Rerun to get cross-references right.' $*.log ; do \
	  $(LATEX) $< || { rm -f $*.pdf $*.dvi $*.aux $*.idx && false ; } ; done

%.bbl: %.bib %.aux
	bibtex $* || { rm -f $*.pdf $*.dvi $*.aux $*.bbl && false ; }

node-selection/optimum-selection-probabilities.pdf \
node-selection/relative-selection-probabilities.pdf: node-selection/plot-node-selection.R
	cd node-selection; R CMD BATCH --vanilla ../$<

node-selection/%.pdf: node-selection/%.R
	cd node-selection; R CMD BATCH --vanilla ../$<

%.pdf: %.R
	R CMD BATCH --vanilla $<

hillclimbing:
	cd node-selection; ./hillclimbing.py node-bw.dat

clean:
	rm -f *~ \
	      *.Rout \
	      *.aux *.log *.out *.bbl *.blg *.pdf
	rm -f node-selection/*.pdf node-selection/*.Rout  node-selection/*~

.PHONY: all hillclimbing clean
