20:09  armadev> sjmurdoch: one topic we might also cover is that there's debate in the systems world about whether tcp end-to-end (alice to exit) is better or worse than a bunch 
                of pairwise tcp connections strung together (alice to 1, 1 to 2, 2 to 3)
20:26  armadev> sjmurdoch: another topic to cover is how many of the congestion control / performance optimizations in tor are actually orthogonal to the choice of transport. 
                e.g. bandwidth measurement and load balancing, or rate limiting, or the circuit priority designs, or mike's circuit build timeout

http://en.wikipedia.org/wiki/Micro_Transport_Protocol